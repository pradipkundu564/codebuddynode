const { Mongoose } = require('mongoose');
const Joi = require('joi');
const mongoose = require('mongoose');
const Post = require('../schema/post.schema');
const { post, response } = require('../app');

function validateData(data) {
    const postSchema = Joi.object({
        title: Joi.string()
            .min(10)
            .required(),
        description: Joi.string().min(50).required()
    }).options({ abortEarly: false })

    return postSchema.validateAsync(data);
}

module.exports.createPost = (req, res) => {
    try {

        const { userId, title, description } = req.body;

        if (!mongoose.Types.ObjectId.isValid(userId)) {
            res.send({ error: "Invalid user" });
        } else {
            let validatorObject = {
                title: title.split("").join(),
                description: description.split("").join()
            }

            validateData(validatorObject).then(async valid => {
                let postCreated = await Post.create(req.body);
                res.send(postCreated)
            }).catch(error => {
                res.send(error.details[0].message);
            })
        }
    } catch (error) {
        res.send({ error: error.message });
    }
}