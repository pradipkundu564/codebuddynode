const User = require('../schema/user.schema');

module.exports.getUsersWithPostCount = async (req, res) => {
    try {
        User.aggregate([
            {
                $lookup: {
                    from: "posts",
                    localField: "_id",
                    foreignField: "userId",
                    as: "posts"
                }
            }
        ]).then(users=>{
            res.send({ users });
        })
    } catch (error) {
        res.send({error: error.message});
    }
}